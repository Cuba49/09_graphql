import { useQuery } from "react-apollo";
import Header from "./components/Header";
import { MESSAGE_QUERY } from "./quries";

function App() {
	const { loading, error, data } = useQuery(MESSAGE_QUERY, {
		variables: {},
	});
	if (loading) return null;
	if (error) return `Error! ${error}`;
	console.log(data);
	return (
		<div className="App">
			<div className="body">
				<Header />
				{data}
				dfgvfgfd
			</div>
		</div>
	);
}

export default App;
