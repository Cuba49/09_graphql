import gql from "graphql-tag";

export const MESSAGE_QUERY = gql`
	query messageQuery(
		$orderBy: MessageOrderByInput
		$filter: String
		$skip: Int
		$first: Int
	) {
		messages(orderBy: $orderBy, filter: $filter, skip: $skip, first: $first) {
			messageList {
				id
				title
				likes
				dislikes
				comments {
					id
					title
					likes
					dislikes
				}
			}
			count
		}
	}
`;
