const { GraphQLServer } = require('graphql-yoga');
const { prisma } = require('./generated/prisma-client')
const Query = require('./resolvers/Query');
const Mutation = require('./resolvers/Mutations');
const Subscription = require('./resolvers/Subscription')
const Message = require('./resolvers/Message')
const Comment = require('./resolvers/Comment')

const resolvers = {
    Query,
    Mutation,
    Subscription, 
    Message,
    Comment
};

const server = new GraphQLServer({
    typeDefs: 'server/schema.graphql',
    resolvers,
    context: {prisma}
});

server.start(()=>console.log('http://localhost:4000'))