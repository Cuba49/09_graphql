function postMessage(parent, args, context, info) {
    return context.prisma.createMessage({
        title: args.title,
        likes:0,
        dislikes:0
    })
}
async function postComment(parent, args, context, info){
    const messageExist= await context.prisma.$exists.message({
        id: args.messageId
    });
    if(!messageExist){
        throw new Error(`Message doesn't exist(${args.messageId})`)
    }

    return context.prisma.createComment({
        title:args.title,
        message:{connect:{id:args.messageId}},
        likes:0,
        dislikes:0
    })
}

module.exports = {
    postMessage,
    postComment
}