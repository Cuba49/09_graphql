async function messages(parent, args, context) {
	const where = args.filter
		? {
				title: args.filter,
		  }
		: {};
	const messageList = await context.prisma.messages({
		where,
		skip: args.skip,
		first: args.first,
		orderBy: args.orderBy,
	});
	const count = await context.prisma
		.messagesConnection({ where })
		.aggregate()
		.count();
	return {
		messageList,
		count,
	};
}
async function comments(parent, args, context) {
	const comments = await context.prisma.comments();
	return comments;
}

module.exports = {
	messages,
	comments,
};
