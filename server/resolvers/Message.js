function comments(parent, args, context){
    return context.prisma.message({
        id:parent.id
    }).comments();
}
module.exports={
    comments
}