function message(parent,args,context){
    return context.prisma.comments({
        id:parent.id
    }).message();
}
module.exports={
    message
}